# Laravel Application README

## Exercise 1: Understanding Laravel Basics

### Questions
1. **What is Laravel?**
   - a. Un langage de programmation
   - b. **Un framework PHP**
   - c. Un serveur web

2. **What is Eloquent ORM in Laravel?**
   - a. Un moteur de template pour générer du HTML
   - b. Une bibliothèque pour gérer les sessions utilisateur
   - c. **Une couche d'abstraction pour interagir avec la base de données**

3. **What is a middleware in Laravel?**
   - a. Une couche d'abstraction pour interagir avec la base de données
   - b. Une fonctionnalité pour gérer les sessions utilisateur
   - c. **Une couche intermédiaire pour traiter les requêtes HTTP avant qu'elles n'atteignent le contrôleur**

4. **What is WordPress?**
   - a. Une plate-forme de blogging
   - b. **Un système de gestion de contenu open-source**
   - c. Un outil d'analyse Web

## Exercise 2: Laravel Commands and Middleware Usage

### Middleware "auth"
- **Role:**
  - Restricts access to authenticated users.
  - Redirects non-authenticated users to the default Laravel login page.
- **Usage:**
  ```php
  Route::middleware('auth')->group(function () {
      // Routes accessible only to authenticated users
  });
### Middleware "guest"
- **Role:**
  - Restricts access to certain routes to non-authenticated users.
  - Typically used for login pages to prevent already authenticated users from accessing them.
- **Usage:**
  ```php
  Route::middleware('guest')->group(function () {
    // Routes accessible only to non-authenticated users
  });

## Exercise 3: Do more !!
1. **run the server ?**
    - php artisan serve
2. **get all routes list ?**
    - php artisan route:list
3. **Créer un modèle Eloquent avec son contrôleur de ressource correspondant ?**
    - php artisan make:model NameOfThe --all


   ![Alt text](image.png)


   ![Alt text](image-1.png)

